/*
 * grunt-updater
 *
 *
 * Copyright (c) 2014 heimi
 * Licensed under the MIT license.
 */

'use strict';

module.exports = function(grunt) {

    // Please see the Grunt documentation for more information regarding task
    // creation: http://gruntjs.com/creating-tasks

    function format_json(obj) {
        return JSON.stringify(obj, null, 4);
    }

    function format_jbuilder(obj, lvl) {

        var result = '';

        lvl = lvl || 0;

        Object.keys(obj).forEach(function(key) {

            var v = obj[key],
                type = (typeof v);

            if (type === 'object') {
                result = result + separator(lvl) + 'json.' + key + ' do\n';
                result += format_jbuilder(v, (lvl + 1));
                result = result + separator(lvl) + 'end\n';
            } else if (type === 'string') {
                result = result + separator(lvl) + 'json.' + key + ' "' + v + '"\n';
            } else {
                result = result + separator(lvl) + 'json.' + key + ' ' + v + '\n';
            }

        });

        return result;

    }

    function separator(lvl, str) {

        var sep = '';

        str = str || '\t';

        while (lvl--) {
            sep += str;
        }
        return sep;
    }

    grunt.registerMultiTask('updater', 'Generate updater JSON file for ROC framework.', function() {

        var options = {
            format: 'json' // jbuilder
        },
        formats = {
            json: format_json,
            jbuilder: format_jbuilder
        };

        // Iterate over all specified file groups.
        this.files.forEach(function(f) {

            if (!f.dest) {
                grunt.log.warn('No destination specified for updater.');
                return false;
            }

            var data,
                format = f.format || options.format,
                now = new Date().getTime(),
                json = {};

            json.name = f.name;
            json.version = f.version;
            if (f.downloads) {
                json.downloads = f.downloads;
            }
            json.date = now;

            data = formats[format](json);

            grunt.file.write(f.dest, data);
            grunt.log.writeln('File "' + f.dest + '" created.');

        });

    });

};
